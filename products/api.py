from flask import Flask, Response, request
import json

app = Flask(__name__)

@app.route("/cats")
def cats():
	
	msg = {
		'message': 'Cats'
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp


@app.route("/dogs/<id>")
def dog(id):
	msg = {
		'message': 'Dogs',
		'data': {
			'value': id
		}
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp

@app.route("/products", methods=['GET'])
def list_products():
	msg = {
		'message': 'Showing list of products',
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp

@app.route("/products", methods=['POST'])
def add_products():
	## FORM DATA
	# name = request.form['name']
	# description = request.form['description']
	# types = request.form['types']
	# price = request.form['price']

	## JSON Request
	content = request.get_json(silent=True)
	print content
	
	name = content['name']
	description = content['description']
	types = content['types']
	price = content['price']
	
	msg = {
		'message': 'Add a new product',
		'data': {
			'name': name,
			'description': description,
			'types': types,
			'price': price,
		}
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp

@app.route("/products/<id>", methods=['GET'])
def show_products(id):
	msg = {
		'message': 'Showing one of products - '+id,
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp

@app.route("/products/<id>", methods=['PUT'])
def edit_products(id):
	# name = request.form['name']
	# description = request.form['description']
	# types = request.form['types']
	# price = request.form['price']

	content = request.get_json(silent=True)
	print content
	
	name = content['name']
	description = content['description']
	types = content['types']
	price = content['price']
	
	msg = {
		'message': 'Updating selected products - '+id,
		'data': {
			'name': name,
			'description': description,
			'types': types,
			'price': price,
		}
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp

@app.route("/products/<id>", methods=['DELETE'])
def delete_products(id):
	msg = {
		'message': 'Deleting selected products - '+id,
	}

	resp = Response(response=json.dumps(msg),
				    status=200, \
				    mimetype="application/json")

	return resp