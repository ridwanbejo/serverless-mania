import boto3
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
            return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")
table = dynamodb.Table('Movies')

def hello(event, context):
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))

    # Use this code if you don't use the http event with the LAMBDA-PROXY integration
    """
    return {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "event": event
    }
    """

def movies(event, context):
    results = table.scan()

    response = {
        "statusCode": 200,
        "body": {
            "message": 'Get movies data...',
            "data": results['Items'],
            "length": results['Count']
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))

def movie(event, context):
    result = table.get_item(
            Key = {
                'year': int(event['pathParameters']['year']),
                'title': event['pathParameters']['title'],
        })

    response = {
        "statusCode": 200,
        "body": {
            "message": 'Get movie data...',
            "data": result['Item'],
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))

def search_movies(event, context):
    v_filter = event['queryStringParameters']['filter']
    v_val = event['queryStringParameters']['q']

    if (v_filter == 'title'):
        results = table.scan(
                FilterExpression="contains(title, :val)",
                ExpressionAttributeValues= { ":val": v_val},
            )
    elif (v_filter == 'plot'):
        results = table.scan(
                FilterExpression="contains(info.plot, :val)",
                ExpressionAttributeValues= { ":val": v_val},
            )
    elif (v_filter == 'actor'):
        results = table.scan(
                FilterExpression="contains(info.actors, :val)",
                ExpressionAttributeValues= { ":val": v_val},
            )
    elif (v_filter == 'genre'):
        results = table.scan(
                FilterExpression="contains(info.genres, :val)",
                ExpressionAttributeValues= { ":val": v_val},
            )

    response = {
        "statusCode": 200,
        "body": {
            "message": 'Search movies data...',
            "data": results['Items'],
            "length": results['Count']
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))


def add_movie(event, context):
    params = event['body']
    params['year'] = int(params['year'])
    params['info']['rating'] = decimal.Decimal(params['info']['rating'])

    results = table.put_item(Item=params)

    response = {
        "statusCode": 200,
        "body": {
            "message": 'Get movies data...',
            "data": params,
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))

def delete_movie(event, context):
    result = table.delete_item(
            Key = {
                'year': int(event['pathParameters']['year']),
                'title': event['pathParameters']['title'],
        })

    response = {
        "statusCode": 204,
        "body": {
            "message": 'Delete movie data...',
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))

def edit_info_movie(event, context):
    data = event['body']
    data['rating'] = decimal.Decimal(data['rating'])

    result = table.update_item(
            Key = {
                'year': int(event['pathParameters']['year']),
                'title': event['pathParameters']['title'],
            },
            UpdateExpression="set info = :info",
            ExpressionAttributeValues= {
              ":info": data,
            },
            ReturnValues="UPDATED_NEW"
        )

    response = {
        "statusCode": 200,
        "body": {
            "message": 'Edit info movie success...',
        }
    }

    return (json.dumps(response, indent=4, cls=DecimalEncoder))
