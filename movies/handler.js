'use strict';

const AWS = require('aws-sdk');
AWS.config.update({
   region: "us-west-2",
   endpoint: "http://localhost:8000",
});

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.hello = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      data: {
        'name':'Ridwan Fadjar Septian',
        'company': 'Ebizu Sdn. Bhd.',
        'position': 'Web Developer',
        'favorite' : {
          'music': ['metal', 'rock', 'jazz'],
          'anime': ['One Piece', 'Naruto', 'One Punch Man']
        }
      }
    }),
  };

  callback(null, response);

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};

module.exports.movies = (event, context, callback) => {
  const params = {
    TableName: 'Movies',
  };

  return dynamoDb.scan(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Get movies data...',
          data: data.Items,
          length: data.Count
        }),
      };

      context.succeed(response);
  });
};

module.exports.movie = (event, context, callback) => {
  
  const params = {
    TableName: "Movies",
    Key: {
      "year": parseInt(event.pathParameters.year),
      "title": event.pathParameters.title,
    }
  };

  console.log(event);
  
  return dynamoDb.get(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Get movie data...',
          data: data.Item,
        }),
      };

      context.succeed(response);
  });
};

module.exports.searchMovies = (event, context, callback) => {
  
  var filter = event.queryStringParameters.filter;

  var params = {
    TableName: "Movies",
    
  }

  if (filter == 'title')
  {
      params.FilterExpression = "contains(title, :val)";
  }
  else if (filter == 'plot')
  {
      params.FilterExpression = "contains(info.plot, :val)";
  }
  else if (filter == 'genre')
  {
      params.FilterExpression = "contains(info.genres, :val)";
  }
  else if (filter == 'actor')
  {
      params.FilterExpression = "contains(info.actors, :val)";
  }
  
  params.ExpressionAttributeValues = {
        ":val": event.queryStringParameters.q,
      };

  return dynamoDb.scan(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      console.log(data);

      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Search movie data...',
          data: data.Items,
          length: data.Count
        }),
      };

      context.succeed(response);
  });
};

module.exports.addMovie = (event, context, callback) => {
  const data = JSON.parse(event.body);
  console.log(data);
   
  var params = {
    TableName: "Movies",
    Item: data,
  };

  params.Item.year = parseInt(data.year);

  return dynamoDb.put(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      console.log(data);

      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Add movie...',
          data: params.Item,
        }),
      };

      context.succeed(response);
  });
};

module.exports.editInfoMovie = (event, context, callback) => {
  const data = JSON.parse(event.body);
  console.log(data);

  const params = {
    TableName: "Movies",
    Key: {
      "year": parseInt(event.pathParameters.year),
      "title": event.pathParameters.title,
    },
    UpdateExpression: "set info = :info",
    ExpressionAttributeValues: {
      ":info": data,
    },
    ReturnValues:"UPDATED_NEW"
  };

  console.log(params);

  return dynamoDb.update(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Edit movie success...',
          data: data
        }),
      };

      context.succeed(response);
  });
};

module.exports.deleteMovie = (event, context, callback) => {
  const params = {
    TableName: "Movies",
    Key: {
      "year": parseInt(event.pathParameters.year),
      "title": event.pathParameters.title,
    }
  };

  return dynamoDb.delete(params, (error, data) => {
      if (error) {
        console.log(error);

        callback(error);
      }
      
      const response = {
        statusCode: 204,
        body: JSON.stringify({
          message: 'Delete movie success...',
        }),
      };

      context.succeed(response);
  });
};

