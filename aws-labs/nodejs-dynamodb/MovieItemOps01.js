var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
   region: "us-west-2",
   endpoint: "http://localhost:8000",
});

var docClient = new AWS.DynamoDB.DocumentClient();
var params = {
	TableName: "Movies",
	Item: {
		"year": 2015,
		"title": "The Big New Movie in The Earth",
		"info":{
			"plot": "Nothing happens at all",
			"rating": 5
		}
	}
};

console.log("Adding a new item...");

docClient.put(params, function(err, data){
	if (err)
	{
		console.error("Error: ", JSON.stringify(err, null, 2));
	}
	else 
	{
		console.log("Success: ", JSON.stringify(data, null, 2));
	}
});