var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
   region: "us-west-2",
   endpoint: "http://localhost:8000",
});

var docClient = new AWS.DynamoDB.DocumentClient();

var params = {
	TableName: "Movies",
	ProjectionExpression:"#yr, title, info.genres, info.actors[0]",
	KeyConditionExpression: "#yr = :yyyy and title between :letter1 and :letter2",
	ExpressionAttributeNames: {
		"#yr": "year"
	},
	ExpressionAttributeValues:{
		":yyyy": 1990,
		":letter1": "A",
		":letter2": "Z",
	}
}

docClient.query(params, function(err, data) {
	if (err) {
		console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
	} else {
		console.log("Query succeeded.");
		data.Items.forEach(function(item) {
			console.log(" -", item.year + ": " + item.title
			+ " ... " + item.info.genres
			+ " ... " + item.info.actors[0]);
		});
	}
});