var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
   region: "us-west-2",
   endpoint: "http://localhost:8000",
});

var docClient = new AWS.DynamoDB.DocumentClient();
var params = {
	TableName: "Movies",
	Key: {
		"year": 2015,
		"title": "The Big New Movie in The Earth",
	}
};

console.log("Adding a new item...");

docClient.get(params, function(err, data){
	if (err)
	{
		console.error("Error: ", JSON.stringify(err, null, 2));
	}
	else 
	{
		console.log("Success: ", JSON.stringify(data, null, 2));
	}
});