var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
   region: "us-west-2",
   endpoint: "http://localhost:8000",
});

var docClient = new AWS.DynamoDB.DocumentClient();
var params = {
	TableName: "Movies",
	Key: {
		"year": 2015,
		"title": "The Big New Movie in The Earth",
	},
	UpdateExpression: "set info.rating = info.rating + :r",
	ExpressionAttributeValues: {
		":r": 2,
	},
	ReturnValues:"UPDATED_NEW"
};

console.log("Updating the item...");

docClient.update(params, function(err, data){
	if (err)
	{
		console.error("Error: ", JSON.stringify(err, null, 2));
	}
	else 
	{
		console.log("Success: ", JSON.stringify(data, null, 2));
	}
});