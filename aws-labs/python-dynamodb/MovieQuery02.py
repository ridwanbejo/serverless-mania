from __future__ import print_function
from boto3.dynamodb.conditions import Key, Attr

import boto3
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, decimal.Decimal):
			if o % 1 > 0:
				return float(o)
			else:
				return int(o)
			return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")
table = dynamodb.Table('Movies')

response = table.query(
		ProjectionExpression = "#yr, title, info.genres, info.actors[0]",
		ExpressionAttributeNames= { "#yr": "year"},
		KeyConditionExpression = Key('year').eq(1992) & Key('title').between('A', 'L')
	)

for i in response['Items']:
	print(json.dumps(i, cls=DecimalEncoder))