from __future__ import print_function
import boto3
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, decimal.Decimal):
			if o % 1 > 0:
				return float(o)
			else:
				return int(o)
			return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")
table = dynamodb.Table('Movies')

response = table.update_item(
			Key = {
				'year': 2015,
				'title': "The Big New Movie",
			},
			UpdateExpression="set info.rating = :r, info.plot = :p, info.actors = :a",
			ExpressionAttributeValues={
				':r': decimal.Decimal(5.5),
				':p': "Everything happens all at once.",
				':a': ["Larry", "Moe", "Curly"]
			},
			ReturnValues="UPDATED_NEW"
		)

print (json.dumps(response, indent=4, cls=DecimalEncoder))