from __future__ import print_function
from botocore.exceptions import ClientError
import boto3
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, decimal.Decimal):
			if o % 1 > 0:
				return float(o)
			else:
				return int(o)
			return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")
table = dynamodb.Table('Movies')

try:
	response = table.update_item(
			Key = {
				'year': 2015,
				'title': "The Big New Movie",
			},
			UpdateExpression="remove info.actors[0]",
			ConditionExpression="size(info.actors) >= :num",
			ExpressionAttributeValues={
				':num': 3,
			},
			ReturnValues="UPDATED_NEW"
		)


except ClientError as e:
	print (e.response['Error']['Message'])
else:
	print (json.dumps(response, indent=4, cls=DecimalEncoder))
